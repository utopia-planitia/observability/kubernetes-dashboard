apiVersion: v1
kind: Secret
metadata:
  name: basic-auth
  namespace: kubernetes-dashboard
type: Opaque
stringData:
  auth: "{{ .Values.basic_auth }}"
